morsegen (0.2.1-5) unstable; urgency=medium

  * bumped Standards-Version: 4.7.0
  * applied Helmut Grohne's debdiff, thanks! Closes: #1086543

 -- Georges Khaznadar <georgesk@debian.org>  Fri, 08 Nov 2024 15:33:10 +0100

morsegen (0.2.1-4) unstable; urgency=medium

  * adopted the package. Closes: #920109
  * bumped debhelper-compat to 13, Standards-Version to 4.5.1

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 18 Jan 2022 15:37:00 +0100

morsegen (0.2.1-3) unstable; urgency=medium

  * QA upload.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.5.0.
      - Update Homepage field.
      - Update maintainer field to Debian QA Group.
      - Update Vcs fields to use Salsa.
  * debian/copyright:
      - Added packaging rights for me.
      - Updated Source field.
      - Updated the upstream copyright years.
      - Updated Upstream-Name field.
  * debian/patches/10-morsegen.c.patch: updated the header.
  * debian/rules: rearranged and removed unnecessary content.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/tests/*: created to provide a trivial CI test.
  * debian/upstream.changelog: removed, no longer needed.
  * debian/watch:
      - Updated to show the last version in upstream git repository.
      - Updated to version 4.

 -- Thiago Andrade Marques <thmarques@gmail.com>  Tue, 25 Feb 2020 21:25:54 -0300

morsegen (0.2.1-2) UNRELEASED; urgency=medium

  * debian/compat
    - Update to 9.
  * debian/control
    - (Build-Depends): Update to debhelper 9.
    - (Homepage): Update.
    - (Standards-Version): Update to 3.9.8.
    - (Vcs-*): Update to anonscm.debian.org.
  * debian/copyright
    - (Format): https
    - Update to Format 1.0.
  * debian/pod2man.mk
    - Make build reproducible (Closes: 782219).
      Patch thanks to Reiner Herrmann <reiner@reiner-h.de>.
  * debian/rules
    - Add hardening.

 -- Jari Aalto <jari.aalto@cante.net>  Wed, 19 Oct 2016 13:51:01 +0300

morsegen (0.2.1-1) unstable; urgency=low

  * Initial release (Closes: #583358).

 -- Jari Aalto <jari.aalto@cante.net>  Sat, 13 Nov 2010 18:40:24 +0200
